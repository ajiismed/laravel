<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <p>First name :</p>
            <input type="text" name="namadepan"/>
        <p>Last name :</p>
            <input type="text" name="namabelakang"/>
        <p>Gender : </p>
            <label><input type="radio" name="jenis_kelamin" value="pria">Male</label></br>
            <label><input type="radio" name="jenis_kelamin" value="wanita">Female</label></br>
            <label><input type="radio" name="jenis_kelamin" value="lain-lain">Other</label></br>
        <p>Nationality :</p>
            <select name="Negara">
                <option value="indonesia">Indonesia</option>
                <option value="amerika">Amerika</option>
                <option value="inggris">Inggris</option>
            </select>
        <p>Language Spoken </p>
            <label><input type="checkbox" name="bahasa" value="bahasa_indonesia">Bahasa Indonesia</label></br>
            <label><input type="checkbox" name="bahasa" value="bahasa_inggris">Enghlish</label></br>
            <label><input type="checkbox" name="bahasa" value="lain-lain">Other</label></br>
        <p>Bio :</p>
            <textarea cols="30" rows="5"></textarea></body></br>
            <input type="submit" value="kirim">
    </form>

</body>
</html>